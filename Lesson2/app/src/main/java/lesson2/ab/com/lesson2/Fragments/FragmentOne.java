package lesson2.ab.com.lesson2.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lesson2.ab.com.lesson2.R;

public class FragmentOne extends BaseFragment {

    private static final String TITLE = "One";

    public static BaseFragment getInstance() {
        return new FragmentOne();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_one, container, false);
    }

    @Override
    public String getTitle() {
        return TITLE;
    }
}
