package lesson2.ab.com.lesson2.Fragments;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment implements iTabFragment {
    @Override
    public abstract String getTitle();
}
