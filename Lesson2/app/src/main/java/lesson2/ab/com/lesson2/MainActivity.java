package lesson2.ab.com.lesson2;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import lesson2.ab.com.lesson2.Adapters.PagerAdapter;
import lesson2.ab.com.lesson2.Fragments.BaseFragment;
import lesson2.ab.com.lesson2.Fragments.FragmentOne;
import lesson2.ab.com.lesson2.Fragments.FragmentThree;
import lesson2.ab.com.lesson2.Fragments.FragmentTwo;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), getFragments());
        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_one: viewPager.setCurrentItem(0); break;
            case R.id.item_two: viewPager.setCurrentItem(1); break;
            case R.id.item_three: viewPager.setCurrentItem(2); break;
            default: return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(FragmentOne.getInstance());
        list.add(FragmentTwo.getInstance());
        list.add(FragmentThree.getInstance());
        return list;
    }
}
