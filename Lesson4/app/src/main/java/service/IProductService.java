package service;

import java.util.List;

import data.model.ProductEntity;

public interface IProductService {

    List<ProductEntity> getAll();

    ProductEntity getProduct(long id);

    long addProduct(ProductEntity entity);

    void updateProduct(ProductEntity entity);

    void deleteProduct(ProductEntity entity);

}
