package service.impl;

import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import data.db.ProductDao;
import data.model.ProductEntity;
import scheduler.IScheduler;
import service.IProductService;

public class ProductService implements IProductService {

    private static final String TAG = "PRODUCT_SERVER";

    private static IProductService mInstance;
    private ProductDao mDao;
    private IScheduler mScheduler;

    private long mProductId;
    private List<ProductEntity> mProducts;
    private ProductEntity mProduct;

    private ProductService(ProductDao dao, IScheduler scheduler) {
        mDao = dao;
        mScheduler = scheduler;
    }

    public static IProductService getInstance(ProductDao productDao, IScheduler scheduler) {
        if (mInstance == null) {
            mInstance = new ProductService(productDao, scheduler);
        }

        return mInstance;
    }

    @Override
    public List<ProductEntity> getAll() {
        Future future = runWithFuture( () -> mProducts = mDao.getAll() );
        wait(future);
        Log.d(TAG,  "amount: " + mProducts.size());
        return mProducts;
    }

    @Override
    public ProductEntity getProduct(long id) {
        Future future = runWithFuture(() -> mProduct = mDao.getById(id));
        wait(future);
        return mProduct;
    }

    @Override
    public long addProduct(ProductEntity entity) {
        Future future = runWithFuture(() -> mProductId = mDao.insert(entity));
        wait(future);
        Log.d(TAG,  "id: " + mProductId);
        return mProductId;
    }

    @Override
    public void updateProduct(ProductEntity product) {
        runWithFutureAndWait(() -> mDao.update(product));
    }

    @Override
    public void deleteProduct(ProductEntity product) {
        runWithFutureAndWait(() -> mDao.delete(product));
    }

    // todo: refactor the methods below

    private void runOnScheduler(Runnable runnable) {
        mScheduler.runOnThread(runnable);
    }

    private void runWithFutureAndWait(Runnable runnable) {
        Future future = runWithFuture(runnable);
        wait(future);
    }

    private Future runWithFuture(Runnable runnable) {
        return mScheduler.runWithFuture(runnable);
    }

    private void wait(Future future) {
        try {
            future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
