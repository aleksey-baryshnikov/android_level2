package service.impl;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import data.db.UserDao;
import data.model.UserEntity;
import data.prefs.IPreferences;
import scheduler.IScheduler;
import service.IUserService;

public class UserService implements IUserService {

    private static IUserService mInstance;
    private static UserDao mDao;
    private static IPreferences mPreference;
    private static IScheduler mScheduler;
    private UserEntity mUser;
    private long mUserId;

    private UserService(UserDao dao, IPreferences preference, IScheduler scheduler) {
        mDao = dao;
        mPreference = preference;
        mScheduler = scheduler;
    }

    public static IUserService getInstance(UserDao dao, IPreferences preferences, IScheduler scheduler) {
        if (mInstance == null) {
            mInstance = new UserService(dao, preferences, scheduler);
        }

        return mInstance;
    }

    @Override
    public boolean isAuth() {
        return mPreference.isAuth();
    }

    @Override
    public void saveId(long id) {
        mPreference.saveUserId(id);
    }

    @Override
    public long getId() {
        return mPreference.getUserId();
    }

    @Override
    public UserEntity getUser(long id) {
        Future future = runWithFuture(() -> mUser = mDao.getById(id));
        wait(future);
        return mUser;
    }

    @Override
    public long saveUser(UserEntity entity) {
        Future future = runWithFuture(() -> mUserId = mDao.insert(entity));
        wait(future);
        // do we need to store this id into shared preferences ?
        return mUserId;
    }

    @Override
    public void updateUser(UserEntity entity) {
        runOnScheduler(() -> mDao.update(entity));
    }

    @Override
    public void deleteUser(UserEntity entity) {
        runOnScheduler(() -> mDao.delete(entity));
    }

    @Override
    public void signOut(UserEntity entity) {
        deleteUser(entity);
        mPreference.clear();
    }

    // implementation

    private void runOnScheduler(Runnable runnable) {
        mScheduler.runOnThread(runnable);
    }

    private Future runWithFuture(Runnable runnable) {
        return mScheduler.runWithFuture(runnable);
    }

    private void wait(Future future) {
        try {
            future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
