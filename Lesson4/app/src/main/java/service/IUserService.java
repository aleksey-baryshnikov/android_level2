package service;

import data.model.UserEntity;

public interface IUserService {

    boolean isAuth();

    void saveId(long id);

    long getId();

    UserEntity getUser(long id);

    long saveUser(UserEntity entity);

    void updateUser(UserEntity entity);

    void deleteUser(UserEntity entity);

    void signOut(UserEntity entity);
}
