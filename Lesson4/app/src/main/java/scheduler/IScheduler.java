package scheduler;

import java.util.concurrent.Future;

public interface IScheduler {
    void runOnThread(Runnable runnable);
    Future runWithFuture(Runnable runnable);
}
