package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import data.SourceProvider;
import data.model.UserEntity;
import lesson4_homework.ab.com.lesson4.R;
import service.IUserService;

public class ProfileActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    //

    private IUserService mUserService;
    private UserEntity mUser;
    private TextView mTextFirstName;
    private TextView mTextSecondName;
    private Button mButtonSignOut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        setUp();
    }

    private void init() {
        mUserService = SourceProvider.getUserService(this);
        mTextFirstName = findViewById(R.id.text_first_name);
        mTextSecondName = findViewById(R.id.text_second_name);
        mButtonSignOut = findViewById(R.id.button_sign_out);
    }

    private void setUp() {
        mUser = mUserService.getUser(mUserService.getId());
        mTextFirstName.setText(mUser.getFirstName());
        mTextSecondName.setText(mUser.getLastName());

        mButtonSignOut.setOnClickListener(b -> {
            mUserService.signOut(mUser);
            startActivity(SplashActivity.getIntent(this));
            finish();
        });
    }
}
