package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import data.SourceProvider;
import data.model.ProductEntity;
import lesson4_homework.ab.com.lesson4.R;
import service.IProductService;
import service.impl.ProductService;

public class AddProductActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, AddProductActivity.class);
    }

    //

    EditText mEditProductName;
    Button mButtonAddProduct;
    IProductService mProductService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        init();
        setup();
    }

    private void init() {
        mEditProductName = findViewById(R.id.edit_product_name);
        mButtonAddProduct = findViewById(R.id.button_add_product);
        mProductService = SourceProvider.getProductService(this);
    }

    private void setup() {
        mButtonAddProduct.setOnClickListener(this::onAddProductButtonClicked);
    }

    private void onAddProductButtonClicked(View view) {
        String productName = mEditProductName.getText().toString();
        ProductEntity productEntity = new ProductEntity(productName);
        mProductService.addProduct(productEntity);
        finish();
    }
}
