package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.xml.transform.Source;

import data.SourceProvider;
import data.model.UserEntity;
import lesson4_homework.ab.com.lesson4.R;
import service.IUserService;

public class RegistrationActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, RegistrationActivity.class);
    }

    //

    private IUserService mUserService;

    private EditText mEditEmail;
    private EditText mEditPassword;
    private EditText mEditFirstName;
    private EditText mEditSecondName;
    private Button mButtonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        init();
        setUp();
    }

    private void init() {
        mUserService = SourceProvider.getUserService(this);
        mEditEmail = findViewById(R.id.edit_email);
        mEditPassword = findViewById(R.id.edit_password);
        mEditFirstName = findViewById(R.id.edit_first_name);
        mEditSecondName = findViewById(R.id.edit_second_name);
        mButtonSignUp = findViewById(R.id.button_sign_up);
    }

    private void setUp() {
        mButtonSignUp.setOnClickListener(this::signUpClicked);
    }

    private void signUpClicked(View view) {
        String email = mEditEmail.getText().toString();
        String password = mEditPassword.getText().toString();
        String firstName = mEditFirstName.getText().toString();
        String secondName = mEditSecondName.getText().toString();

        UserEntity user = new UserEntity();
        user.setEmail(email);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(secondName);

        long id = mUserService.saveUser(user);
        mUserService.saveId(id);
        startActivity(MainActivity.getIntent(this));
        finish();
    }
}
