package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import data.SourceProvider;
import data.model.ProductEntity;
import lesson4_homework.ab.com.lesson4.R;
import service.IProductService;

public class EditProductActivity extends AppCompatActivity {

    private static final String EXTRA_PRODUCT_ID = "lesson4_homework.ab.com.lesson4.view.EXTRA_PRODUCT_ID";

    public static Intent getIntent(Context context, long productId) {
        Intent intent = new Intent(context, EditProductActivity.class);
        intent.putExtra(EXTRA_PRODUCT_ID, productId);
        return intent;
    }

    //

    private IProductService mProductService;
    private long mProductId;

    private EditText mEditProductName, mEditDescription, mEditSupplier, mEditCountry, mEditQuantity, mEditPrice;
    private Button mButtonSave, mButtonDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        init();
        try {
            setup();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        mProductService = SourceProvider.getProductService(this);
        mEditProductName = findViewById(R.id.edit_product_name);
        mEditDescription = findViewById(R.id.edit_product_description);
        mEditSupplier = findViewById(R.id.edit_product_supplier);
        mEditCountry = findViewById(R.id.edit_product_country);
        mEditQuantity = findViewById(R.id.edit_product_quantity);
        mEditPrice = findViewById(R.id.edit_product_price);
        mButtonSave = findViewById(R.id.button_save_product);
        mButtonDelete = findViewById(R.id.button_delete_product);
    }

    private void setup() throws Exception {
        mProductId = getIntent().getExtras().getLong(EXTRA_PRODUCT_ID);

        if (mProductId <= 0) {
            throw new Exception("ProductId should be provided");
        }

        ProductEntity product = mProductService.getProduct(mProductId);

        mEditProductName.setText(product.getName());
        mEditDescription.setText(product.getDescription());
        mEditSupplier.setText(product.getSupplier());
        mEditCountry.setText(product.getCountry());
        mEditQuantity.setText(Integer.toString(product.getQuantity()));
        mEditPrice.setText(Integer.toString(product.getPrice()));

        mButtonSave.setOnClickListener(this::saveProduct);
        mButtonDelete.setOnClickListener(this::deleteProduct);
    }

    private void saveProduct(View view) {
        ProductEntity product = mProductService.getProduct(mProductId);

        product.setName(mEditProductName.getText().toString());
        product.setDescription(mEditDescription.getText().toString());
        product.setSupplier(mEditSupplier.getText().toString());
        product.setCountry(mEditCountry.getText().toString());
        product.setQuantity(Integer.parseInt(mEditQuantity.getText().toString()));
        product.setPrice(Integer.parseInt(mEditPrice.getText().toString()));

        mProductService.updateProduct(product);

        finish();
    }

    private void deleteProduct(View view) {
        // todo: do you really want to delete this product ?
        ProductEntity product = mProductService.getProduct(mProductId);
        mProductService.deleteProduct(product);
        finish();
    }


}
