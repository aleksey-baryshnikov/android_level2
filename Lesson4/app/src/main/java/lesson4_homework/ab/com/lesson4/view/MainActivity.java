package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.List;

import data.SourceProvider;
import data.adapters.ProductsAdapter;
import data.model.ProductEntity;
import lesson4_homework.ab.com.lesson4.R;
import service.IProductService;

public class MainActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    //

    private IProductService mProductService;

    private Button mButtonProfile, mButtonAddProfile;
    private RecyclerView mRecyclerView;
    private ProductsAdapter mAdapter;

    private List<ProductEntity> mProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setUp();
    }

    private void init() {
        mButtonProfile = findViewById(R.id.button_profile);
        mButtonAddProfile = findViewById(R.id.button_add_product);
        mRecyclerView = findViewById(R.id.recycler_view);

        mProductService = SourceProvider.getProductService(this);
        mAdapter = new ProductsAdapter(mProductService.getAll());
    }

    private void setUp() {
        mButtonProfile.setOnClickListener(button -> startActivity(ProfileActivity.getIntent(this)));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnListViewItemClickedListener(this::onProductClicked);

        mButtonAddProfile.setOnClickListener(button -> startActivity(AddProductActivity.getIntent(this)));
    }

    private void onProductClicked(int index) {
        ProductEntity product = mProductService.getAll().get(index);
        Intent intent = EditProductActivity.getIntent(this, product.getId());
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.onProductListUpdated(mProductService.getAll());
    }
}
