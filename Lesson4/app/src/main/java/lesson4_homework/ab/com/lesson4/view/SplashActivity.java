package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import data.SourceProvider;
import lesson4_homework.ab.com.lesson4.R;
import service.IUserService;

public class SplashActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    //

    private IUserService mUserService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mUserService = SourceProvider.getUserService(this);

        Intent intent = mUserService.isAuth()
                ? AuthenticationActivity.getIntent(this)
                : RegistrationActivity.getIntent(this);

        startActivity(intent);
        finish();
    }
}
