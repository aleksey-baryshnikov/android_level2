package lesson4_homework.ab.com.lesson4.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import data.SourceProvider;
import data.model.UserEntity;
import lesson4_homework.ab.com.lesson4.R;
import service.IUserService;

public class AuthenticationActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        return new Intent(context, AuthenticationActivity.class);
    }

    //

    private IUserService mUserService;
    private TextView mTextEmail;
    private EditText mEditPassword;
    private Button mButtonSignIn;
    private UserEntity mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        init();
        setUp();
    }

    private void init() {
        mUserService = SourceProvider.getUserService(this);
        mTextEmail = findViewById(R.id.text_email);
        mEditPassword = findViewById(R.id.edit_password);
        mButtonSignIn = findViewById(R.id.button_sign_in);
        mUser = mUserService.getUser(mUserService.getId());
    }

    private void setUp() {
        mTextEmail.setText(mUser.getEmail());
        mButtonSignIn.setOnClickListener(this::signInClicked);
    }

    private void signInClicked(View view) {
        String password = mEditPassword.getText().toString();
        if (password.equals(mUser.getPassword())) {
            startActivity(MainActivity.getIntent(this));
        } else {
            mEditPassword.setError("Invalid password");
        }
    }
}
