package data.prefs;

public interface IPreferences {

    boolean isAuth();

    long getUserId();

    void saveUserId(long id);

    void clear();

}
