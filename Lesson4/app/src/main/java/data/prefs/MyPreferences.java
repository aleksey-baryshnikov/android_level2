package data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferences implements IPreferences {

    private static final String SHARED_TITLE = "lesson4_homework.ab.com.lesson4.SHARED_TITLE";
    private static final String USER_ID_KEY = "lesson4_homework.ab.com.lesson4.USER_ID_KEY";

    private static IPreferences mInstance;
    private SharedPreferences mSharedPreferences;


    private MyPreferences(Context context) {
        mSharedPreferences = context.getSharedPreferences(SHARED_TITLE, Context.MODE_PRIVATE);
    }

    public static IPreferences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyPreferences(context);
        }

        return mInstance;
    }

    @Override
    public boolean isAuth() {
        return getUserId() > 0;
    }

    @Override
    public long getUserId() {
        return mSharedPreferences.getLong(USER_ID_KEY, -1);
    }

    @Override
    public void saveUserId(long id) {
        exec(editor -> editor.putLong(USER_ID_KEY, id));
    }

    @Override
    public void clear() {
        exec(SharedPreferences.Editor::clear);
    }

    private void exec(EditorRunnable runnable) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        runnable.run(editor);
        editor.apply();
    }

    interface EditorRunnable{
        void run(SharedPreferences.Editor editor);
    }
}
