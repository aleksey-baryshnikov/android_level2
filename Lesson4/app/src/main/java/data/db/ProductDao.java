package data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import data.model.ProductEntity;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM products")
    List<ProductEntity> getAll();

    @Query("SELECT * FROM products WHERE id = :id")
    ProductEntity getById(long id);

    @Insert
    long insert(ProductEntity entity);

    @Update
    void update(ProductEntity entity);

    @Delete
    void delete(ProductEntity entity);

}
