package data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import data.model.ProductEntity;
import data.model.UserEntity;

@Database(entities = {UserEntity.class, ProductEntity.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract ProductDao productDao();

}
