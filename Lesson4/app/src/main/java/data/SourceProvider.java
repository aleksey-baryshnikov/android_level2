package data;

import android.content.Context;

import data.prefs.MyPreferences;
import scheduler.SingleThreadScheduler;
import service.IProductService;
import service.IUserService;
import service.impl.ProductService;
import service.impl.UserService;

public class SourceProvider {

    public static IUserService getUserService(Context context) {
        return UserService.getInstance(
                App.getDb(context).userDao(),
                MyPreferences.getInstance(context),
                SingleThreadScheduler.getInstance());
    }

    public static IProductService getProductService(Context context) {
        return ProductService.getInstance(
                App.getDb(context).productDao(),
                SingleThreadScheduler.getInstance());
    }

}
