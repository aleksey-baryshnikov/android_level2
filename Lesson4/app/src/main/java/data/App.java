package data;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import data.db.AppDatabase;

public class App extends Application {

    private AppDatabase mDatabase;

    private final String DATABASE_NAME = "Lesson4_Database";

    public void onCreate() {
        super.onCreate();
        mDatabase = Room.databaseBuilder(this, AppDatabase.class, DATABASE_NAME).build();
    }

    private static App getInstance(Context context) {
        return (App) context.getApplicationContext();
    }

    public static AppDatabase getDb(Context context) {
        return getInstance(context).mDatabase;
    }
}
