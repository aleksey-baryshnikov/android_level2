package data.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import data.model.ProductEntity;
import lesson4_homework.ab.com.lesson4.R;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    private static final String TAG = "ProductsAdapter";

    private List<ProductEntity> mProducts;

    private IListViewItemClickedListener mListViewItemClickedListener;

    public ProductsAdapter(List<ProductEntity> products) {
        mProducts = products;
    }

    public void setOnListViewItemClickedListener(IListViewItemClickedListener listener) {
        mListViewItemClickedListener = listener;
    }

    public void onProductListUpdated(List<ProductEntity> updatedProducts) {
        mProducts = updatedProducts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_product_list_item, viewGroup, false);
        ProductViewHolder viewHolder = new ProductViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        ProductEntity product = mProducts.get(i);
        productViewHolder.textProductName.setText(product.getName());
    }

    @Override
    public int getItemCount() {
        return mProducts == null ? 0 : mProducts.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        private TextView textProductName;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            textProductName = itemView.findViewById(R.id.text_product_name);
            itemView.setTag(this);
            itemView.setOnClickListener(this::onItemClickListener);
        }

        private void onItemClickListener(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            Log.d(TAG, "Adapter position: " + position);
            if (mListViewItemClickedListener != null) {
                mListViewItemClickedListener.OnViewItemClicked(position);
            }
        }
    }

    public interface IListViewItemClickedListener{
        void OnViewItemClicked(int position);
    }
}
