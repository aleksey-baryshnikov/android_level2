package lesson2homework.ab.com.lesson2_homework.Fragments;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public abstract String getTitle();
}
