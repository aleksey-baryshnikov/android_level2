package lesson2homework.ab.com.lesson2_homework;

import android.content.DialogInterface;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import lesson2homework.ab.com.lesson2_homework.Adapters.MyPageAdapter;
import lesson2homework.ab.com.lesson2_homework.Fragments.BaseFragment;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentFive;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentFour;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentOne;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentSeven;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentSix;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentThree;
import lesson2homework.ab.com.lesson2_homework.Fragments.FragmentTwo;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "TAG_MainActivity";

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MyPageAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindControls();

        setSupportActionBar(toolbar);
        pagerAdapter = new MyPageAdapter(getSupportFragmentManager(), getFragments());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int position = -1;

        switch (item.getItemId()) {
            case R.id.item_one: position = 0; break;
            case R.id.item_two: position = 1; break;
            case R.id.item_three: position = 2; break;
            case R.id.item_four: position = 3; break;
            case R.id.item_five: position = 4; break;
            case R.id.item_six: position = 5; break;
            case R.id.item_seven: position = 6; break;
        }

        showAlert(position);

        if (position >= 0) {
            viewPager.setCurrentItem(position);
        }

        return position >= 0 ? true : super.onOptionsItemSelected(item);
    }

    private void bindControls() {
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(FragmentOne.createInstance());
        list.add(FragmentTwo.createInstance());
        list.add(FragmentThree.createInstance());
        list.add(FragmentFour.createInstance());
        list.add(FragmentFive.createInstance());
        list.add(FragmentSix.createInstance());
        list.add(FragmentSeven.createInstance());
        return list;
    }

    private void showAlert(int position) {
        String message = "Item clicked: " +  String.valueOf(position);

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }

        builder
            .setTitle("Homework 2")
            .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "positive button clicked");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "negative button clicked");
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
