package lesson2homework.ab.com.lesson2_homework.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lesson2homework.ab.com.lesson2_homework.R;

public class FragmentOne extends BaseFragment {

    private static final String TITLE = "1";

    public static BaseFragment createInstance() {
        return new FragmentOne();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_one, container, false);
    }

    @Override
    public String getTitle() {
        return TITLE;
    }
}
