package lesson2homework.ab.com.lesson2_homework.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lesson2homework.ab.com.lesson2_homework.R;

public class FragmentSeven extends BaseFragment {

    private static final String TITLE = "7";

    public static BaseFragment createInstance() {
        return new FragmentSeven();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_seven, container, false);
    }

    @Override
    public String getTitle() {
        return TITLE;
    }
}
