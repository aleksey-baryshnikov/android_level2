package lesson1home.ab.com.lesson1_homework;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import lesson1home.ab.com.lesson1_homework.Fragments.FragmentFive;
import lesson1home.ab.com.lesson1_homework.Fragments.FragmentFour;
import lesson1home.ab.com.lesson1_homework.Fragments.FragmentOne;
import lesson1home.ab.com.lesson1_homework.Fragments.FragmentThree;
import lesson1home.ab.com.lesson1_homework.Fragments.FragmentTwo;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "TagMainActivity";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggleButton = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggleButton);
        toggleButton.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        log(menuItem.getTitle().toString());

        Class fragmentClass = null;

        switch (menuItem.getItemId()) {
            case R.id.item1:
                fragmentClass = FragmentOne.class;
                break;
            case R.id.item2:
                fragmentClass = FragmentTwo.class;
                break;
            case R.id.item3:
                fragmentClass = FragmentThree.class;
                break;
            case R.id.item4:
                fragmentClass = FragmentFour.class;
                break;
            case R.id.item5:
                fragmentClass = FragmentFive.class;
                break;
        }

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container_fragment, fragment)
                .commit();

        drawerLayout.closeDrawers();
        return true;
    }

    public static void log(String message) {
        Log.d(TAG, message);
    }
}
