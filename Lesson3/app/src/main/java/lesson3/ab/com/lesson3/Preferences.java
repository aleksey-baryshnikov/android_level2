package lesson3.ab.com.lesson3;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences implements IPreferences {

    private static final String STORAGE_TITLE = "lesson3.ab.com.lesson.STORAGE_TITLE";
    private static final String EMAIL_KEY = "lesson3.ab.com.lesson.EMAIL_KEY";
    private static final String PASSWORD_KEY = "lesson3.ab.com.lesson.PASSWORD_KEY";
    private static Preferences mInstance;

    private SharedPreferences mPreferences;

    private Preferences(Context context) {
        mPreferences = context.getSharedPreferences(STORAGE_TITLE, Context.MODE_PRIVATE);
    }

    public static IPreferences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Preferences(context);
        }
        return mInstance;
    }

    @Override
    public boolean contains(String key) {
        return mPreferences.contains(EMAIL_KEY);
    }

    @Override
    public String getEmail() {
        return getStringValue(EMAIL_KEY);
    }

    @Override
    public void saveEmail(String email) {
        exec(editor -> editor.putString(EMAIL_KEY, email));
    }

    @Override
    public String getPassword() {
        return getStringValue(PASSWORD_KEY);
    }

    @Override
    public void savePassword(final String password) {
        /*
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PASSWORD_KEY, password);
        editor.apply();
        */

        exec(editor -> editor.putString(PASSWORD_KEY, password));
    }

    @Override
    public void clear() {
        exec(editor -> editor.clear());
    }

    private String getStringValue(String key) {
        return mPreferences.getString(key, "");
    }

    private void exec(PreferencesOperation call) {
        SharedPreferences.Editor editor = mPreferences.edit();
        call.operation(editor);
        editor.apply();
    }

    @FunctionalInterface
    interface PreferencesOperation {
        void operation(SharedPreferences.Editor editor);
    }
}
