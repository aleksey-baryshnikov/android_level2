package lesson3.ab.com.lesson3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    private IPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mPreferences = Preferences.getInstance(this);

        boolean isAuth = mPreferences.contains(AuthActivity.AUTH_KEY);
        startActivity(AuthActivity.getIntent(this, isAuth));
        finish();
    }
}
