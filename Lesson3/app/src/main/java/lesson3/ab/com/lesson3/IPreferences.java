package lesson3.ab.com.lesson3;

public interface IPreferences {
    boolean contains(String key);
    String getEmail();
    void saveEmail(String email);
    String getPassword();
    void savePassword(String password);
    void clear();
}
