package lesson3.ab.com.lesson3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button mButtonSignOut;
    IPreferences preferences;

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonSignOut = findViewById(R.id.btn_sign_out);
        preferences = Preferences.getInstance(this);
        mButtonSignOut.setOnClickListener(this::SignOut);
    }

    private void SignOut(View view) {
        preferences.clear();
        startActivity(AuthActivity.getIntent(this, false));
        finish();
    }
}
